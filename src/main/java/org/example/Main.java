package org.example;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class Main {
    public static void main(String[] args) throws IOException, ParseException {

        Scanner scanner = new Scanner(System.in);

        String pattern = "yyyyMMdd";
        String pattern2 = "dd-MM-yyyy";

        boolean active = true;

        while (active){
            System.out.println("Введите дату (пример ввода: 2-2-2000 или 02-02-2000) или \"Выход\": ");
            String myDatetime = scanner.next();

            if(myDatetime.toLowerCase().equals("выход"))
                break;

            String[] splitted = myDatetime.split("-");

            List<String> restrictions = new ArrayList<>();

            try {
                if(splitted.length == 3  && splitted[0].length() < 3 && splitted[1].length() < 3){
                    for(int n = 0; n < splitted.length; n++){
                        Integer.parseInt(splitted[n]);
                        if(n == 2){
                            SimpleDateFormat format2 = new SimpleDateFormat(pattern2, Locale.ENGLISH);
                            Date myDate = format2.parse(myDatetime);

                            BufferedReader br = new BufferedReader(new FileReader("csvFile.csv"));

                            List<String> data = new LinkedList<>();

                            for(var str : br.lines().toList()){
                                data.add(List.of(str.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)")).toString());
                            }

                            data.remove(0);


                            data.forEach(el -> {
                                String[] res = el.split(", ");
                                for (int i = 0; i < res.length; i++){

                                    String datetimeStart;
                                    String datetimeEnd;
                                    Date dateStart;
                                    Date dateEnd;

                                    if(Pattern.matches("^[0-9]{8}$", res[i]) && Pattern.matches("^[0-9]{8}$", res[i - 1]) == false && Pattern.matches("^[0-9]{8}$", res[i - 2]) == false) {
                                        datetimeStart = res[i];
                                        if(res[i + 1] != "-")
                                            datetimeEnd = res[i + 1];
                                        else
                                            datetimeEnd = java.time.LocalTime.now().toString();

                                        SimpleDateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);

                                        try {
                                            dateStart = format.parse(datetimeStart);
                                            dateEnd = format.parse(datetimeEnd);
                                        } catch (ParseException e) {
                                            throw new RuntimeException(e);
                                        }

                                        if ((myDate.after(dateStart) || myDate.compareTo(dateStart) == 0) && (myDate.before(dateEnd) || myDate.compareTo(dateEnd) == 0)){
                                            if(res[i - 2].equals("-") == false)
                                                restrictions.add(res[i - 2]);

                                        }

                                    }

                                }

                            });
                        }

                    }

                    SimpleDateFormat format2 = new SimpleDateFormat(pattern2, Locale.ENGLISH);
                    Date myDate = format2.parse(myDatetime);

                    System.out.printf("%1$s %2$td %2$tB %2$tY\n", "Дата:", myDate);
                    Map map = restrictions.stream().collect(groupingBy(Function.identity(),counting()));
                    for(int i = 0; i < map.size(); i++){
                        System.out.println(map.keySet().toArray()[i] + " - " + map.get(map.keySet().toArray()[i]));
                    }

                    System.out.printf("%1$s %2$td %2$tB %2$tY", "Всего ограничений на", myDate);
                    System.out.println(": " + restrictions.size());

                }
                else
                    System.out.println("Неверный ввод");


            }
            catch (NumberFormatException e) {
                System.out.println("Неверный ввод");
            }
        }

    }


}
